import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const _2b7d330b = () => import('../pages/tests/index.vue' /* webpackChunkName: "pages/tests/index" */).then(m => m.default || m)
const _01806fd0 = () => import('../pages/login.vue' /* webpackChunkName: "pages/login" */).then(m => m.default || m)
const _608e004b = () => import('../pages/home/index.vue' /* webpackChunkName: "pages/home/index" */).then(m => m.default || m)
const _07092acf = () => import('../pages/talentslist/index.vue' /* webpackChunkName: "pages/talentslist/index" */).then(m => m.default || m)
const _85b59d12 = () => import('../pages/about/index.vue' /* webpackChunkName: "pages/about/index" */).then(m => m.default || m)
const _624d62ec = () => import('../pages/contact/index.vue' /* webpackChunkName: "pages/contact/index" */).then(m => m.default || m)
const _fb7b079a = () => import('../pages/tests/_id.vue' /* webpackChunkName: "pages/tests/_id" */).then(m => m.default || m)
const _4718bec3 = () => import('../pages/talent/_id/index.vue' /* webpackChunkName: "pages/talent/_id/index" */).then(m => m.default || m)
const _26e5c829 = () => import('../pages/dashboard/_id/index.vue' /* webpackChunkName: "pages/dashboard/_id/index" */).then(m => m.default || m)
const _3753183a = () => import('../pages/talentlistbyexpertises/_id/index.vue' /* webpackChunkName: "pages/talentlistbyexpertises/_id/index" */).then(m => m.default || m)
const _37d8ccb9 = () => import('../pages/index.vue' /* webpackChunkName: "pages/index" */).then(m => m.default || m)



if (process.client) {
  window.history.scrollRestoration = 'manual'
}
const scrollBehavior = function (to, from, savedPosition) {
  // if the returned position is falsy or an empty object,
  // will retain current scroll position.
  let position = false

  // if no children detected
  if (to.matched.length < 2) {
    // scroll to the top of the page
    position = { x: 0, y: 0 }
  } else if (to.matched.some((r) => r.components.default.options.scrollToTop)) {
    // if one of the children has scrollToTop option set to true
    position = { x: 0, y: 0 }
  }

  // savedPosition is only available for popstate navigations (back button)
  if (savedPosition) {
    position = savedPosition
  }

  return new Promise(resolve => {
    // wait for the out transition to complete (if necessary)
    window.$nuxt.$once('triggerScroll', () => {
      // coords will be used if no selector is provided,
      // or if the selector didn't match any element.
      if (to.hash) {
        let hash = to.hash
        // CSS.escape() is not supported with IE and Edge.
        if (typeof window.CSS !== 'undefined' && typeof window.CSS.escape !== 'undefined') {
          hash = '#' + window.CSS.escape(hash.substr(1))
        }
        try {
          if (document.querySelector(hash)) {
            // scroll to anchor by returning the selector
            position = { selector: hash }
          }
        } catch (e) {
          console.warn('Failed to save scroll position. Please add CSS.escape() polyfill (https://github.com/mathiasbynens/CSS.escape).')
        }
      }
      resolve(position)
    })
  })
}


export function createRouter () {
  return new Router({
    mode: 'history',
    base: '/',
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-exact-active',
    scrollBehavior,
    routes: [
		{
			path: "/tests",
			component: _2b7d330b,
			name: "tests"
		},
		{
			path: "/login",
			component: _01806fd0,
			name: "login"
		},
		{
			path: "/home",
			component: _608e004b,
			name: "home"
		},
		{
			path: "/talentslist",
			component: _07092acf,
			name: "talentslist"
		},
		{
			path: "/about",
			component: _85b59d12,
			name: "about"
		},
		{
			path: "/contact",
			component: _624d62ec,
			name: "contact"
		},
		{
			path: "/tests/:id",
			component: _fb7b079a,
			name: "tests-id"
		},
		{
			path: "/talent/:id?",
			component: _4718bec3,
			name: "talent-id"
		},
		{
			path: "/dashboard/:id?",
			component: _26e5c829,
			name: "dashboard-id"
		},
		{
			path: "/talentlistbyexpertises/:id?",
			component: _3753183a,
			name: "talentlistbyexpertises-id"
		},
		{
			path: "/",
			component: _37d8ccb9,
			name: "index"
		}
    ],
    
    
    fallback: false
  })
}
