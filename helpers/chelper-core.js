const CHelperCore = {
  Test() {
    return 'CHelper'
  },

  SetUser(user) {
    user["sesstionStartTime"] = new Date().toString()
    let u = JSON.stringify(user)
    window.localStorage.setItem('apptoken', u)
  },

  ClearUser() {
    if (window.localStorage.getItem("apptoken") !== null) {
      window.localStorage.removeItem('apptoken')
    }
  },

  GetUser() {

    if (window.localStorage.getItem("apptoken") != null && window.localStorage.getItem("apptoken") !== 'undefined') {
      let user = window.localStorage.getItem("apptoken")
      user = JSON.parse(user)

      let currentTime = Date.parse(new Date())
      let diffMs = (currentTime - Date.parse(user.sesstionStartTime))
      let diffMins = parseInt(Math.round(((diffMs % 86400000) % 3600000) / 60000)) // minutes

      if (diffMins >= user.expireIn) {
        ClearUser()
        location.href = 'auth/login'
      }
      return user
    }
    return null;
  }
}

export default CHelperCore
