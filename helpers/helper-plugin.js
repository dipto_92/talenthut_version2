import {Promise} from 'es6-promise'
import CHelperCore from '~/helpers/chelper-core.js'
import toastr from "toastr"


export default {
    install: (Vue, options) => {
        Vue.prototype.$Helpers = {

            ApiBaseUrl() {
                return options.apiBaseUrl
            },

            ShowSuccess(message) {
                toastr.success(message, 'Success!')
            },
            ShowError(message) {
                toastr.error(message, 'Error!')
            },
            ShowInfo(message) {
                toastr.info(message, 'info')
            },

            _HandleMessage(response, showMessage) {

                if (response && showMessage) {
                    if (response && response.status) {

                        if (response.status === true) {
                            this.ShowSuccess(response.message)
                        }
                        else if (response.status === false) {
                            this.ShowError(response.message)
                        }

                    }
                }
            },

            Post(url, data, token, showMessage = true, async = true) {
                return new Promise((resolve, reject) => {
                    $.ajax({
                        type: 'POST',
                        async: async,
                        url: options.apiBaseUrl + url,
                        /*beforeSend: function (xhdr) {
                            xhdr.setRequestHeader("Authorization", "Bearer " + token);
                        },*/
                        data: data,
                        success: (response, textStatus, jqXHR) => {
                            this._HandleMessage(response, showMessage)
                            resolve(response);
                        },
                        error: (jqXHR, textStatus, errorThrown) => {
                            reject(errorThrown);
                        }
                    });
                });
            },

            PostFormData(url, data, token, showMessage = true) {
                return new Promise((resolve, reject) => {
                    $.ajax({
                        type: 'POST',
                        url: options.apiBaseUrl + url,
                        dataType: 'json',
                        contentType: false,
                        processData: false,
                        /*beforeSend: function (xhdr) {
                            xhdr.setRequestHeader("Authorization", "Bearer " + token);
                        },*/
                        data: data,
                        success: (response, textStatus, jqXHR) => {
                            //_HandleMessage(response, showMessage)
                            resolve(response);
                        },
                        error: (jqXHR, textStatus, errorThrown) => {
                            reject(errorThrown);
                        }
                    });
                });
            },

            Get(url, data, token, showMessage = false, async = true) {
                return new Promise((resolve, reject) => {
                    $.ajax({
                        type: 'GET',
                        async: async,
                        url: options.apiBaseUrl + url,
                        /*beforeSend: function (xhdr) {
                            xhdr.setRequestHeader("Authorization", "Bearer " + token);
                        },*/
                        data: data,
                        success: (response, textStatus, jqXHR) => {
                            //_HandleMessage(response, showMessage)
                            resolve(response);
                        },
                        error: (jqXHR, textStatus, errorThrown) => {
                            reject(errorThrown);
                        }
                    });
                });
            },

            Delete(url, data, token, showMessage = true, async = true) {
                return new Promise((resolve, reject) => {
                    $.ajax({
                        type: 'DELETE',
                        async: async,
                        url: options.apiBaseUrl + url,
                        /*beforeSend: function (xhdr) {
                            xhdr.setRequestHeader("Authorization", "Bearer " + token);
                        },*/
                        data: data,
                        success: (response, textStatus, jqXHR) => {
                            this._HandleMessage(response, showMessage)
                            resolve(response);
                        },
                        error: (jqXHR, textStatus, errorThrown) => {
                            reject(errorThrown);
                        }
                    });
                });
            },

            IsNullOrEmpty(str) {
                if (!str)
                    return true;
                str = str.toString()
                return str === null || str.match(/^ *$/) !== null;
            },

            ObjectToFormData(obj) {

                var formData = new FormData();
                for (var prop in obj) {
                    if (!obj.hasOwnProperty(prop)) continue;


                    if (obj[prop] !== null && obj[prop] !== 'undefined') {
                        formData.append(prop, obj[prop]);
                    }
                }
                return formData;
            },

            IsSuccess(response) {
                if (response) {
                    if (response.hasOwnProperty('message')) {
                        let message = response.message
                        if (message.MessageType === 1) {
                            return true
                        }
                        else if (message.MessageType === 2) {
                            return false
                        }
                    }

                }
                return false
            },
            permission_check(route, permissions) {

                let check = false;

                permissions.forEach(function (value, key) {
                    if (value.route == route && value.is_permission == 1) {
                        check = true
                    }
                });

                return check

            },

            Put(url, data, token, showMessage = true, async = true) {
                return new Promise((resolve, reject) => {
                    $.ajax({
                        type: 'PUT',
                        async: async,
                        url: options.apiBaseUrl + url,
                        /*beforeSend: function (xhdr) {
                            xhdr.setRequestHeader("Authorization", "Bearer " + token);
                        },*/
                        data: data,
                        success: (response, textStatus, jqXHR) => {
                            this._HandleMessage(response, showMessage)
                            resolve(response);
                        },
                        error: (jqXHR, textStatus, errorThrown) => {
                            reject(errorThrown);
                        }
                    });
                });
            },
        }
    }
};
