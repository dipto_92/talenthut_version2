module.exports = {
    /*
    ** Headers of the page
    */
   generate: {
        routes: [
          '/talent/1',
          '/talent/2',
          '/talent/40'
        ]
  },
    
    mode:'spa',

    head: {
        title: 'TALENT HUT',
        meta: [
            { charset: 'utf-8' }
        ],
        link: [
        ],
        script: [
            { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js' },
            { src: 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js'}
        ]
    },

    css: [
    ],


    /*
    ** Customize the progress bar color
    */
    loading: { color: '#3B8070' },
    /*
    ** Build configuration
    */
    build: {
        vendor: [],
        extend (config, { isDev, isClient }) {
            if (isDev && isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/
                })
            }
        }
    },
    plugins: [
        { src: '~/plugins/bootstrap/css/bootstrap.css',ssr:false},
        { src: '~/plugins/helper', ssr: false },
        { src: '~/plugins/chelper', ssr: false }
    ]
}
