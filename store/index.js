import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'
Vue.use(Vuex)


const createStore = () => {
    return new Vuex.Store({
        state: {
            isAuthenticated: false,
            token:'',
            user: {
            },
            menuItems: [
            ]
        },
        mutations: {
            SetIdentity(state, response) {
                state.user = response
                state.token = response.token
                state.isAuthenticated = true
            },
            ClearIdentity(state) {
                state.user = {}
                state.token = ''
                state.isAuthenticated = false
            }
        },

        plugins: [(new VuexPersistence({
            storage: window.localStorage
        })).plugin]
    })
}

export default createStore
